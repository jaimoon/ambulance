package com.versatilemobitech.ambulancetracker.login.model;

import com.google.gson.annotations.SerializedName;

public class LoginData {

	@SerializedName("colleg_name")
	private String collegName;

	@SerializedName("profile_image")
	private String profileImage;

	@SerializedName("name")
	private String name;

	@SerializedName("mobile")
	private String mobile;

	@SerializedName("country_name")
	private String countryName;

	@SerializedName("year_id")
	private String yearId;

	@SerializedName("id")
	private String id;

	@SerializedName("university_name")
	private String universityName;

	@SerializedName("email")
	private String email;

	@SerializedName("country_id")
	private String countryId;

	@SerializedName("year_name")
	private String yearName;

	public void setCollegName(String collegName){
		this.collegName = collegName;
	}

	public String getCollegName(){
		return collegName;
	}

	public void setProfileImage(String profileImage){
		this.profileImage = profileImage;
	}

	public String getProfileImage(){
		return profileImage;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setMobile(String mobile){
		this.mobile = mobile;
	}

	public String getMobile(){
		return mobile;
	}

	public void setCountryName(String countryName){
		this.countryName = countryName;
	}

	public String getCountryName(){
		return countryName;
	}

	public void setYearId(String yearId){
		this.yearId = yearId;
	}

	public String getYearId(){
		return yearId;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setUniversityName(String universityName){
		this.universityName = universityName;
	}

	public String getUniversityName(){
		return universityName;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setCountryId(String countryId){
		this.countryId = countryId;
	}

	public String getCountryId(){
		return countryId;
	}

	public void setYearName(String yearName){
		this.yearName = yearName;
	}

	public String getYearName(){
		return yearName;
	}

	@Override
 	public String toString(){
		return 
			"LoginData{" +
			"colleg_name = '" + collegName + '\'' + 
			",profile_image = '" + profileImage + '\'' + 
			",name = '" + name + '\'' + 
			",mobile = '" + mobile + '\'' + 
			",country_name = '" + countryName + '\'' + 
			",year_id = '" + yearId + '\'' + 
			",id = '" + id + '\'' + 
			",university_name = '" + universityName + '\'' + 
			",email = '" + email + '\'' + 
			",country_id = '" + countryId + '\'' + 
			",year_name = '" + yearName + '\'' + 
			"}";
		}
}