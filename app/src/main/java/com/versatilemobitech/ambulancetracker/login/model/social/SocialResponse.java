package com.versatilemobitech.ambulancetracker.login.model.social;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SocialResponse implements Serializable {

	@SerializedName("result")
	private int result;

	@SerializedName("message")
	private String message;

	@SerializedName("data")
	private SocialData data;

	public void setResult(int result){
		this.result = result;
	}

	public int getResult(){
		return result;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setData(SocialData data){
		this.data = data;
	}

	public SocialData getData(){
		return data;
	}

	@Override
 	public String toString(){
		return 
			"SocialResponse{" + 
			"result = '" + result + '\'' + 
			",message = '" + message + '\'' + 
			",data = '" + data + '\'' + 
			"}";
		}
}