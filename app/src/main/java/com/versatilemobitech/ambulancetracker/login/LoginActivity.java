package com.versatilemobitech.ambulancetracker.login;

import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.versatilemobitech.ambulancetracker.R;
import com.versatilemobitech.ambulancetracker.databinding.ActivityLoginBinding;
import com.versatilemobitech.ambulancetracker.interfaces.AActivity;
import com.versatilemobitech.ambulancetracker.otp.OtpVerificationActivity;
import com.versatilemobitech.ambulancetracker.registration.RegistrationActivity;
import com.versatilemobitech.ambulancetracker.utilities.Utility;

public class LoginActivity extends Activity implements AActivity {

    ActivityLoginBinding binding;
    private String phoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      //  setContentView(R.layout.activity_login);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);

        setUp();
        setOnClickListeners();
    }


    private void setUp() {

    }


    private void setOnClickListeners() {

        binding.tvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                phoneNumber = binding.etPhoneNumber.getText().toString().trim();

                if (phoneNumber.isEmpty())
                {
                    showSnackBar("Please enter mobile number",2);
                }
                else if (!(phoneNumber.startsWith("9") || phoneNumber.startsWith("8") || phoneNumber.startsWith("7") ||
                        phoneNumber.startsWith("6") ))
                {
                    showSnackBar("Please enter valid mobile number",3);
                }
                else if (phoneNumber.length() != 10)
                {
                    showSnackBar("Please enter valid mobile number",3);
                }
                else
                {
                    showSnackBar("Seccess",1);
                    Toast.makeText(LoginActivity.this, "Okay", Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(LoginActivity.this, OtpVerificationActivity.class);
                    startActivity(intent);
                }

            }
        });

        binding.tvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(LoginActivity.this, RegistrationActivity.class);
                startActivity(intent);

            }
        });



    }

    @Override
    public void changeTitle(String title) {

    }

    @Override
    public void showSnackBar(String snackBarText, int type) {

        Utility.showSnackBar(this,binding.coordinatorLayout,snackBarText,type);

    }

    @Override
    public Activity activity() {
        return this;
    }
}