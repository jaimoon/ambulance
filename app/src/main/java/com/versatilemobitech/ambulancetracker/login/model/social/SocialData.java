package com.versatilemobitech.ambulancetracker.login.model.social;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SocialData implements Serializable {

	@SerializedName("id")
	private String id;

	@SerializedName("name")
	private String name;

	@SerializedName("email")
	private String email;

	@SerializedName("mobile")
	private String mobile;

	@SerializedName("profile_image")
	private String profileImage;

	@SerializedName("colleg_name")
	private String collegName;

	@SerializedName("university_name")
	private String universityName;

	@SerializedName("year_id")
	private String yearId;

	@SerializedName("year_name")
	private String yearName;

	@SerializedName("country_id")
	private String countryId;

	@SerializedName("country_name")
	private String countryName;

	@SerializedName("is_new_user")
	private String isNewUser;

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setMobile(String mobile){
		this.mobile = mobile;
	}

	public String getMobile(){
		return mobile;
	}

	public void setProfileImage(String profileImage){
		this.profileImage = profileImage;
	}

	public String getProfileImage(){
		return profileImage;
	}

	public void setCollegName(String collegName){
		this.collegName = collegName;
	}

	public String getCollegName(){
		return collegName;
	}

	public void setUniversityName(String universityName){
		this.universityName = universityName;
	}

	public String getUniversityName(){
		return universityName;
	}

	public void setYearId(String yearId){
		this.yearId = yearId;
	}

	public String getYearId(){
		return yearId;
	}

	public void setYearName(String yearName){
		this.yearName = yearName;
	}

	public String getYearName(){
		return yearName;
	}

	public void setCountryId(String countryId){
		this.countryId = countryId;
	}

	public String getCountryId(){
		return countryId;
	}

	public void setCountryName(String countryName){
		this.countryName = countryName;
	}

	public String getCountryName(){
		return countryName;
	}

	public void setIsNewUser(String isNewUser){
		this.isNewUser = isNewUser;
	}

	public String getIsNewUser(){
		return isNewUser;
	}

	@Override
 	public String toString(){
		return 
			"SocialData{" +
			"id = '" + id + '\'' + 
			",name = '" + name + '\'' + 
			",email = '" + email + '\'' + 
			",mobile = '" + mobile + '\'' + 
			",profile_image = '" + profileImage + '\'' + 
			",colleg_name = '" + collegName + '\'' + 
			",university_name = '" + universityName + '\'' + 
			",year_id = '" + yearId + '\'' + 
			",year_name = '" + yearName + '\'' + 
			",country_id = '" + countryId + '\'' + 
			",country_name = '" + countryName + '\'' + 
			",is_new_user = '" + isNewUser + '\'' + 
			"}";
		}
}