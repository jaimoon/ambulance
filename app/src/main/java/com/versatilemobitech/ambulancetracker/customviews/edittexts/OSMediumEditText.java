package com.versatilemobitech.ambulancetracker.customviews.edittexts;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatEditText;

import com.versatilemobitech.ambulancetracker.utilities.Utility;

public class OSMediumEditText extends AppCompatEditText {
    public OSMediumEditText(Context context) {
        super(context);
        init();
    }

    public OSMediumEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public OSMediumEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    private void init() {
        setTypeface(Utility.getTypeface(2,getContext()));
    }

}

