package com.versatilemobitech.ambulancetracker.customviews.textviews;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatEditText;

import com.versatilemobitech.ambulancetracker.utilities.Utility;

/**
 * Created by Gokul Kalagara on 11-07-2018.
 **/

public class OSSemiBoldTextView extends AppCompatEditText
{
    public OSSemiBoldTextView(Context context) {
        super(context);
        init();
    }

    public OSSemiBoldTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public OSSemiBoldTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    private void init() {
        setTypeface(Utility.getTypeface(2,getContext()));
    }

}