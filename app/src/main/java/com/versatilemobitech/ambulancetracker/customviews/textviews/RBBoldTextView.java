package com.versatilemobitech.ambulancetracker.customviews.textviews;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatEditText;

import com.versatilemobitech.ambulancetracker.utilities.Utility;

/**
 * Created by Gokul Kalagara on 11-07-2018.
 **/

public class RBBoldTextView extends AppCompatEditText
{
    public RBBoldTextView(Context context) {
        super(context);
        init();
    }

    public RBBoldTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RBBoldTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    private void init() {
        setTypeface(Utility.getTypeface(7,getContext()));
    }

}