package com.versatilemobitech.ambulancetracker.activities;

import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.versatilemobitech.ambulancetracker.R;
import com.versatilemobitech.ambulancetracker.databinding.ActivityFragmentBinding;

public class FragmentActivity extends androidx.fragment.app.FragmentActivity {

    ActivityFragmentBinding binding;
    RequestQueue requestQueue;

    Fragment fragment;
    private String pageName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //  setContentView(R.layout.activity_fragment);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_fragment);


        requestQueue = Volley.newRequestQueue(this);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setUp();

        setOnClickListeners();

        binding.getRoot();
    }


    private void setUp() {

        fragment = getSupportFragmentManager().findFragmentById(R.id.fragment);

        pageName = getIntent().getExtras().getString("pageName");


        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);

//        if (pageName.equalsIgnoreCase("suggestedvideo")) {
//
//            String id = getIntent().getExtras().getString("id");
//            String titile = getIntent().getExtras().getString("titile");
//
//            Bundle bundle = new Bundle();
//            bundle.putString("pageName", titile);
//            bundle.putString("videoId", id);
//            // binding.tvTitle.setText("Theory");
//            binding.tvTitle.setText(titile);
//            SubCategoryDetailsFragment subCategoryDetailsFragment = new SubCategoryDetailsFragment();
//            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
//            fragmentTransaction.replace(R.id.fragment, subCategoryDetailsFragment);
//            subCategoryDetailsFragment.setArguments(bundle);
//            fragmentTransaction.commit();
//        } else if (pageName.equalsIgnoreCase("subscription")) {
//            binding.tvTitle.setText("SUBSCRIPTION");
//            SunscriptionFragment sunscriptionFragment = new SunscriptionFragment();
//            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
//            fragmentTransaction.replace(R.id.fragment, sunscriptionFragment);
//            fragmentTransaction.commit();
//        }
//
//        else {
//         //   Toast.makeText(this, "hello", Toast.LENGTH_SHORT).show();
//        }

    }

    private void setOnClickListeners() {

        binding.imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });
    }


    public void setTitle(String title) {
       // binding.tvTitle.setText(title);
        binding.tvTitle.setText(title.toUpperCase());

    }

//    public void setToobarBackground() {
//        binding.toolBar.setBackgroundColor(getResources().getColor(R.color.project_color));
//        binding.tvTitle.setTextColor(getResources().getColor(R.color.white));
//
//        binding.imgClose.setImageResource(R.drawable.backwhite);
//
//    }
//
//    public void setToobarBackgroundWhite() {
//        binding.toolBar.setBackgroundColor(getResources().getColor(R.color.white));
//        binding.tvTitle.setTextColor(getResources().getColor(R.color.black));
//
//        binding.imgClose.setImageResource(R.drawable.backblue);
//    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();


        onBackPressed();

    }




}
