package com.versatilemobitech.ambulancetracker.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.versatilemobitech.ambulancetracker.R;
import com.versatilemobitech.ambulancetracker.login.LoginActivity;

public class SpalshActivity extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spalsh);

        setUp();

    }

    private void setUp() {

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

//                if (SharedPreference.getStringPreference(SpalshActivity.this, SharedPreference.LOGINSTATUS) != null &&
//                        SharedPreference.getStringPreference(SpalshActivity.this, SharedPreference.LOGINSTATUS).equalsIgnoreCase("YES")) {
//                    Intent intent = new Intent(SpalshActivity.this, HomeMainActivity.class);
//                    startActivity(intent);
//                    finish();
//                } else {
//                    Intent intent = new Intent(SpalshActivity.this, LoginActivity.class);
//                    startActivity(intent);
//                    finish();
//                }
                Intent intent = new Intent(SpalshActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();


            }
        }, 2500);

    }
}