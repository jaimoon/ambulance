package com.versatilemobitech.ambulancetracker.interfaces;

public interface VersionListener {
    void onGetResponse(String onlineVersion);
}
