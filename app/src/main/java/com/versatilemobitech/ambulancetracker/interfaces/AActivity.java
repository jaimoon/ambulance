package com.versatilemobitech.ambulancetracker.interfaces;

import android.app.Activity;

public interface AActivity {
    public void changeTitle(String title);
    public void showSnackBar(String snackBarText, int type);
    public Activity activity();
}
