package com.versatilemobitech.ambulancetracker.registration;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;

import com.versatilemobitech.ambulancetracker.R;
import com.versatilemobitech.ambulancetracker.databinding.ActivityRegistrationBinding;
import com.versatilemobitech.ambulancetracker.interfaces.AActivity;
import com.versatilemobitech.ambulancetracker.login.LoginActivity;
import com.versatilemobitech.ambulancetracker.otp.OtpVerificationActivity;
import com.versatilemobitech.ambulancetracker.utilities.Utility;

public class RegistrationActivity extends Activity implements AActivity {

    ActivityRegistrationBinding binding;
    private String name,phoneNumber,location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.activity_registration);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_registration);


        setUp();
        setOnClickListeners();

    }



    private void setUp() {


    }

    private void setOnClickListeners() {

        binding.tvContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                name = binding.etName.getText().toString().trim();

                if (name.isEmpty())
                {
                    showSnackBar("Please enter full name",2);
                }
                else if (name.equalsIgnoreCase(null))
                {
                    showSnackBar("Please enter full name",3);
                }
                else
                {
                    showSnackBar("Success",1);
                    Toast.makeText(RegistrationActivity.this, "Okay", Toast.LENGTH_SHORT).show();
                }

                phoneNumber = binding.etPhoneNumber.getText().toString().trim();

                if (phoneNumber.isEmpty())
                {
                    showSnackBar("Please enter mobile number",2);
                }
                else if (!(phoneNumber.startsWith("9") || phoneNumber.startsWith("8") || phoneNumber.startsWith("7") ||
                        phoneNumber.startsWith("6") ))
                {
                    showSnackBar("Please enter valid mobile number",3);
                }
                else if (phoneNumber.length() != 10)
                {
                    showSnackBar("Please enter valid mobile number",3);
                }
                else
                {
                    showSnackBar("Success",1);
                    Toast.makeText(RegistrationActivity.this, "Okay", Toast.LENGTH_SHORT).show();
                }

                location = binding.etLocation.getText().toString().trim();

                if (location.isEmpty())
                {
                    showSnackBar("Please add location",2);
                }
                else if (location.equalsIgnoreCase(null))
                {
                    showSnackBar("Please add location",3);
                }
                else
                {
                    showSnackBar("Success",1);
                    Toast.makeText(RegistrationActivity.this, "Okay", Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(RegistrationActivity.this, OtpVerificationActivity.class);
                    startActivity(intent);
                }

            }
        });

    }

    @Override
    public void changeTitle(String title) {

    }

    @Override
    public void showSnackBar(String snackBarText, int type) {

        Utility.showSnackBar(this,binding.coordinatorLayout,snackBarText,type);
    }

    @Override
    public Activity activity() {
        return this;
    }
}